package com.labsoftware.clinica.controllers;

import com.labsoftware.clinica.models.Consulta;
import com.labsoftware.clinica.services.ConsultaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/clinica/consulta")
public class ConsultaController {

    @Autowired
    ConsultaService consultaService;

    @PostMapping("/cadastrar")
    public Consulta cadastrar(@RequestBody Consulta consulta) {
        return consultaService.cadastrar(consulta);
    }

    @GetMapping("/consultar")
    public Consulta consultar(@RequestParam int busca, @RequestParam String tipo) {
        switch (tipo) {
            case "paciente":
                return consultaService.consultaPorPaciente(busca);
            case "medico":
                return consultaService.consultaPorCrm(busca);
            default:
                return consultaService.consultaPorIdConsulta(busca);
        }
    }

    //TODO: Falta finalizar endpoint de atualizar dados consulta
    @PatchMapping("/alterar/{idConsulta}")
    public Consulta alterar(@PathVariable int idConsulta, @RequestBody Map<String, String> valor) {

        return consultaService.alterar(idConsulta, valor.get("status"));

    }

}
