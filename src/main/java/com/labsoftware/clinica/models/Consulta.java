package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "consulta", indexes = {
        @Index(name = "id_paciente_idx", columnList = "id_paciente"),
        @Index(name = "id_medico_idx", columnList = "id_medico"),
        @Index(name = "id_status_idx", columnList = "id_status")
})
@Entity
@Getter
@Setter
public class Consulta {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "data", nullable = false)
    private LocalDate data;

    @Column(name = "hora_inicio", nullable = false)
    private LocalTime horaInicio;

    @Column(name = "hora_fim", nullable = false)
    private LocalTime horaFim;

    @ManyToOne
    @JoinColumn(name = "id_paciente")
    private Paciente paciente;

    @ManyToOne
    @JoinColumn(name = "id_medico")
    private Medico medico;

    @ManyToOne
    @JoinColumn(name = "id_status")
    private Status status;

}