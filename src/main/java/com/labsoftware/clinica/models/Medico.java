package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "medico")
@Entity
@Setter
public class Medico {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Getter
    @Column(name = "nome", length = 45)
    private String nome;

    @Column(name = "crm")
    private Integer crm;

    @Getter
    @Column(name = "especialidade", length = 45)
    private String especialidade;
}