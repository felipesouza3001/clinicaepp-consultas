package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "paciente", indexes = {
        @Index(name = "id_plano_idx", columnList = "id_plano")
})
@Entity
@Setter
public class Paciente {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Getter
    @Column(name = "nome")
    private String nome;

    @ManyToOne
    @JoinColumn(name = "id_plano")
    private Plano plano;
}