package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "status")
@Entity
@Setter
public class Status {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Getter
    @Column(name = "nome")
    private String nome;
}