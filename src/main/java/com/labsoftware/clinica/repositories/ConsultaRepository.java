package com.labsoftware.clinica.repositories;

import com.labsoftware.clinica.models.Consulta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ConsultaRepository extends CrudRepository<Consulta, Integer> {

    @Query(value= "SELECT * FROM consulta WHERE id_paciente = ?1", nativeQuery = true)
    Optional<Consulta> findByPaciente(Integer id);

    @Query(value= "SELECT * FROM consulta WHERE id_medico in (SELECT id FROM medico WHERE crm = ?1)", nativeQuery = true)
    Optional<Consulta> findByCrm(int crm);

}
