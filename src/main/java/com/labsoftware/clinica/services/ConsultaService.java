package com.labsoftware.clinica.services;

import com.labsoftware.clinica.models.Consulta;
import com.labsoftware.clinica.repositories.ConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ConsultaService {

    @Autowired
    private ConsultaRepository consultaRepository;

    public Consulta cadastrar(Consulta consulta) {
        return consultaRepository.save(consulta);
    }

    //TODO: Melhoria se tiver mais de uma consulta nas buscas da erro
    public Consulta consultaPorPaciente(int idPaciente) {

        Optional<Consulta> retorno = consultaRepository.findByPaciente(idPaciente);

        if (!retorno.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Paciente não existe na base de dados");
        }

        return retorno.get();
    }

    public Consulta consultaPorCrm(int crm) {
        Optional<Consulta> retorno = consultaRepository.findByCrm(crm);

        if (!retorno.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Médico não existe na base de dados");
        }

        return retorno.get();
    }

    public Consulta consultaPorIdConsulta(int idConsulta) {
        Optional<Consulta> retorno = consultaRepository.findById(idConsulta);

        if (!retorno.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Consulta não existe na base de dados");
        }

        return retorno.get();
    }

    public Consulta alterar(int idPaciente, String status) {
        //return pacienteRepository.save(paciente);
        return null;
    }


}
